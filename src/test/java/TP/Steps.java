package TP;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class Steps {
    String urlDeBase = "https://wordpress.com";
    String urlFrancaise = "https://wordpress.com/fr/";
    WebDriver butineur = new FirefoxDriver();
    WebDriverWait attente = new WebDriverWait(butineur, 10);


    Actions ArnoldSchwarzenegger = new Actions(butineur);

    @Given("un navigateur est ouvert")
    public void un_navigateur_est_ouvert() {
        System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
        butineur.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        butineur.get(urlDeBase);
    }

    @And("je suis sur le site")
    public void je_suis_sur_le_site() {
        assertEquals(urlFrancaise, butineur.getCurrentUrl());
        WebElement cookiesBanner = butineur.findElement(By.xpath("//a[text()='Accepter tout']"));
        attente.until(ExpectedConditions.elementToBeClickable(cookiesBanner)).click();
    }

    @And("je choisis {string} dans le menu principal")
    public void dans_le_menu_principal(String sousmenu) {
        WebElement menuPrincipal = butineur.findElement(By.xpath("//button[contains(., 'Fonctionnalités')]"));
        ArnoldSchwarzenegger.moveToElement(menuPrincipal);
        ArnoldSchwarzenegger.click().build().perform();

        WebElement menuPrincipalSousMenu = butineur.findElement(By.xpath("//button[contains(., 'Fonctionnalités')]//following::a[contains(., '"+sousmenu+"')]"));
        attente.until(ExpectedConditions.visibilityOf(menuPrincipalSousMenu));
        ArnoldSchwarzenegger.moveToElement(menuPrincipalSousMenu);
        ArnoldSchwarzenegger.click().build().perform();
    }
    @And("je peux lire le titre {string} de la page demandee {string}")
    public void je_peux_lire_le_titre_de_la_page_demandee(String titrePage, String sousMenu) throws InterruptedException {
        // Write code here that turns the phrase above into concrete actions
        System.out.println(titrePage);
        System.out.println(sousMenu);
        switch (sousMenu) {
            case "Extensions WordPress": {
                WebElement titreH1 = butineur.findElement(By.xpath("//h1[@class='search-box-header__header']"));
                assertEquals(titrePage, attente.until(ExpectedConditions.visibilityOf(titreH1)).getText());
                break;
            }
            case "Thèmes WordPress": {
                Thread.sleep(5000);
                WebElement titreH1 = butineur.findElement(By.xpath("//h1"));
                assertEquals(titrePage, attente.until(ExpectedConditions.visibilityOf(titreH1)).getText());
                break;
            }
            default:
                System.out.println("coin, coin");
                break;
        }


        WebElement inputRecherche = butineur.findElement(By.xpath("//input[contains(@id, 'search-component-')]"));

//        attente.until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
//        WebElement dernierElement = butineur.findElement(By.xpath("//div[@class=\"card theme-card theme-card--is-actionable is-clickable\"][130]"));
        //div[@class="card theme-card theme-card--is-actionable is-clickable"][130]
//        attente.until(ExpectedConditions.elementToBeClickable(dernierElement));
        attente.until(ExpectedConditions.elementToBeClickable(inputRecherche));

//        Thread.sleep(5000);

        WebElement titreH1 = butineur.findElement(By.xpath("//h1[@class='search-box-header__header']"));
        assertEquals(titrePage, attente.until(ExpectedConditions.visibilityOf(titreH1)).getText());
        throw new io.cucumber.java.PendingException();
    }
//    @And("je peux lire le titre {string} de la page demandee {string}")
//    public void je_peux_lire_le_titre_de_la_page(String titrePage, String sousMenu) throws InterruptedException {
//        switch (sousMenu) {
//            case "Extensions WordPress": {
//                WebElement titreH1 = butineur.findElement(By.xpath("//h1[@class='search-box-header__header']"));
//                assertEquals(titrePage, attente.until(ExpectedConditions.visibilityOf(titreH1)).getText());
//                break;
//            }
//            case "Thèmes WordPress": {
//                WebElement titreH1 = butineur.findElement(By.xpath("//h1"));
//                assertEquals(titrePage, attente.until(ExpectedConditions.visibilityOf(titreH1)).getText());
//                break;
//            }
//            default:
//                System.out.println("coin, coin");
//                break;
//        }
//
//
//        Thread.sleep(5000);
//        WebElement inputRecherche = butineur.findElement(By.xpath("//input[contains(@id, 'search-component-')]"));
//
////        attente.until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
////        WebElement dernierElement = butineur.findElement(By.xpath("//div[@class=\"card theme-card theme-card--is-actionable is-clickable\"][130]"));
//        //div[@class="card theme-card theme-card--is-actionable is-clickable"][130]
////        attente.until(ExpectedConditions.elementToBeClickable(dernierElement));
//        attente.until(ExpectedConditions.elementToBeClickable(inputRecherche));
//
////        Thread.sleep(5000);
//
//        WebElement titreH1 = butineur.findElement(By.xpath("//h1[@class='search-box-header__header']"));
//        assertEquals(titrePage, attente.until(ExpectedConditions.visibilityOf(titreH1)).getText());
//    }

    @And("je recherche lelement nomme {string}")
    public void je_recherche_lextention(String element) {
//        WebElement inputRecherche = butineur.findElement(By.xpath("//input[contains(@id, 'search-component-')]"));
        WebElement inputRecherche = butineur.findElement(By.xpath("//input[parent::form[parent::div]]"));

        ArnoldSchwarzenegger.moveToElement(inputRecherche);
        ArnoldSchwarzenegger.click().sendKeys(element).build().perform();

        WebElement buttonRechercher = butineur.findElement(By.xpath("//button[@class='components-button search-component__icon-navigation']"));
        buttonRechercher.click();
    }

    @And("je clique sur lelement {string} dans la liste de resultats")
    public void jeCliqueSurLextensionDansLaListeDeResultats(String element) {
        WebElement extensionRecherchee = butineur.findElement(By.xpath("//div[text()='"+element+"']"));
        attente.until(ExpectedConditions.elementToBeClickable(extensionRecherchee)).click();
    }

    @And("la page de lelement {string} est affichee")
    public void laPageDeLextensionEstAffichee(String element) {
        WebElement detailsExtension = butineur.findElement(By.xpath("//h1[text()='"+element+"']"));
        attente.until(ExpectedConditions.visibilityOf(detailsExtension));
        assertEquals(element, detailsExtension.getText());
    }

    @Then("je clique sur le bouton telecharger")
    public void jeCliqueSurLeBoutonTelecharger() {
        WebElement telecharger = butineur.findElement(By.xpath("//a[@class='button' and text()='Télécharger']"));
        telecharger.click();
    }
}