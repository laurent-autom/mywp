Feature: Télécharger une extension

  Scenario Outline: Affichages multiples
    Given un navigateur est ouvert
    When je suis sur le site
    And je choisis <Sousmenu> dans le menu principal
    And je peux lire le titre <TitrePage> de la page demandee <Sousmenu>
    And je recherche lelement nomme <Element>
    And je clique sur lelement <Element> dans la liste de resultats
    And la page de lelement <Element> est affichee
    Then je clique sur le bouton telecharger

    @AdminProfile
    Examples:
      | Sousmenu               | TitrePage                                                      | Element                                 |
      | "Extensions WordPress" | "Ajoutez des fonctionnalités à votre site avec des extensions" | "Schema & Structured Data for WP & AMP" |
      #| "Extensions WordPress" | "Ajoutez des fonctionnalités à votre site avec des extensions" | "All in One SEO – La meilleure extension WordPress SEO pour améliorer les classements SEO et augmenter le trafic en toute facilité"                     |

