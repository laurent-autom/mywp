Feature: Afficher un theme avec differents objets de navigation

  Scenario Outline: Affichages multiples
    Given un navigateur est ouvert
    When je suis sur le site
    And je choisis <Sousmenu> dans le menu principal
    And je peux lire le titre <TitrePage> de la page demandee <Sousmenu>
    And je recherche lelement nomme <Element>
    And je clique sur lelement <Element> dans la liste de resultats
    And la page de lelement <Element> est affichee

#    And je recherche le theme nomme Pomme
#    And je clique sur le th�me nomme Pomme
#    And je choisis dafficher le site de d�monstration
#    Then je constate laffichage au format Bureau
#    And je constate laffichage au format Tablette

    @AdminProfile
    Examples:
      | Sousmenu           | TitrePage                                       | Element |
      | "Thèmes WordPress" | "Trouvez le thème parfait pour votre site Web." | "Pomme" |


  #And je recherche le thème nommé Pomme
  #And je clique sur le thème nommé Pomme